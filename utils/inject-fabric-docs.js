#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');
const mkdirp = require('mkdirp');

main();

function main() {
    const adfFilteringConfig = require(path.resolve(`${__dirname}/../utils/adf-filtering.json`));
    const adfBlacklist = adfFilteringConfig.blacklist;
    const adfWhitelist = adfFilteringConfig.whitelist;
    const adfJiraDir = path.resolve(`${__dirname}/../content/cloud/jira/platform/apis/document`);
    const adfFabricDir = path.resolve(`${__dirname}/../node_modules/@atlassian/docs-document-format/dist/content`);

    // Copy ADF md files from node_modules to the source dir <projectDir>/content/cloud/jira/platform/apis/document according to adf-filtering.json
    // Inject Fabric data.json to jiracloud.json according to adf-filtering.json
    new Promise(function (resolve, reject) {
        createAdfDir(adfJiraDir);
        resolve(true);
    }).then(function (result) {
        refreshAdfFiles(adfFabricDir, adfJiraDir, adfWhitelist, adfBlacklist);
    }).then(function (result) {
        injectFabricDataJson(adfWhitelist);
    });
}

function createAdfDir(adfJiraDir) {
    rimraf.sync(adfJiraDir, {});
    let subDirs = ['marks', 'nodes'];
    subDirs.forEach(subDir => {
        let absoluteDir = adfJiraDir + '/' + subDir;
        mkdirp.sync(absoluteDir, err => {
            throw new Error('Failed to create dir ' + absoluteDir);
        });
    });
}

function refreshAdfFiles(adfFabricDir, adfJiraDir, adfWhitelist, adfBlacklist) {
    adfWhitelist.forEach(item => {
        let src = path.resolve(`${adfFabricDir}/${item}.md`);
        let dest = path.resolve(`${adfJiraDir}/${item}.md`);
        let content = fs.readFileSync(src, { encoding: 'utf-8' });
        updateContent(content, dest, adfBlacklist);
    });
}

function updateContent(content, dest, adfBlacklist) {
    content = content.replace(/\/cloud\/stride\/apis\/document\//g, '/cloud/jira/platform/apis/document/')
        .replace(/platform:\ stride/g, 'platform: cloud')
        .replace(/product:\ stride/g, 'product: jiracloud');

    // also remove unwanted links
    adfBlacklist.forEach(item => {
        // item example: nodes/mediaGroup
        let chunks = item.split('/');
        let docName = chunks.pop();
        let docParentDir = chunks.pop();

        // Examples:
        // * [mediaGroup](/cloud/jira/platform/apis/document/nodes/mediaGroup)
        // * Node - [mediaGroup](/cloud/jira/platform/apis/document/nodes/mediaGroup)
        //[MediaGroup](/cloud/jira/platform/apis/document/nodes/mediaGroup),

        let linkRegex = `((\\s*)?(\\*|-)(\\s*(Node|Mark)\\s*-)?\\s*)?\\[${docName}]\\(\\/cloud\\/jira\\/platform\\/apis\\/document\\/${docParentDir}\\/${docName}\\)(,)?`;
        content = replaceAll(linkRegex, content, '');
    });

    fs.writeFileSync(dest, content);
}

function replaceAll(regStr, content, replaceValue) {
    let reg = new RegExp(regStr, 'gi');
    return content.replace(reg, replaceValue);
}

function injectFabricDataJson(adfWhitelist) {
    let docsConfig = require(path.resolve(`${__dirname}/../data/jiracloud.json`));
    let fabricConfig = require(path.resolve(`${__dirname}/../node_modules/@atlassian/docs-document-format/dist/data.json`));
    if (docsConfig.navigation.categories[1].subcategories[1].title === 'Document Format') {
        docsConfig.navigation.categories[1].subcategories[1] = fabricConfig;
    } else {
        docsConfig.navigation.categories[1].subcategories.splice(1, 0, fabricConfig);
    }
    let adfDoc = docsConfig.navigation.categories[1].subcategories[1];
    adfDoc.title = 'Document Format';
    let selectedItems = [];
    adfDoc.items.forEach(item => {
        let docPath = item.url.replace('/cloud/stride/apis/document/', '');
        if (adfWhitelist.indexOf(docPath) !== -1) {
            item.url = item.url.replace('/cloud/stride/apis/document/', '/cloud/jira/platform/apis/document/');
            selectedItems.push(item);
        }
    });
    adfDoc.items = selectedItems;

    fs.writeFileSync(path.resolve(`${__dirname}/../data/jiracloud.json`), JSON.stringify(docsConfig, null, 2) + '\n');
}
