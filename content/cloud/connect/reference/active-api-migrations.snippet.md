# Connect Active API migration

The following API Migrations are currently active (where active means their value will can affect API behavior):

|Migration Key|Purpose / Changed behavior|Allowed values|
|---|---|---|
| `gdpr` | Controls whether personal data is provided via the normal Connect APIs. Additionally, by publishing this value in your Marketplace listing you are indicating to Atlassian your readiness for deprecation. |<ul><li>`true`: This means personal data will be excluded from certain APIs. Additionally, it signals to Atlassian that you have completed migration and are ready for the APIs to be deprecated. </li><li>`false`: This means personal data will be included in certain APIs until the end of the deprecation period. Additionally, it signals to Atlassian that you are working on completing the migration.  If you are blocked please let us know by raising a [developer support ticket](https://ecosystem.atlassian.net/servicedesk/customer/portal/14/create/223).</li><li>[omitted]: This means personal data will be included in certain APIs until the end of the deprecation period. Additionally, it signals to Atlassian that you have not started working on completing the migration.</li></ul>|
