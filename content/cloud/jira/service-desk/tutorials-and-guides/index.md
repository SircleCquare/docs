---
title: Tutorials and guides
product: jsdcloud
category: devguide
aliases:
- /cloud/jira/jira-service-desk/tutorials-and-guides.html
- /cloud/jira/jira-service-desk/tutorials-and-guides.md
layout: tutorials-and-guides
date: "2019-04-24"
---

# Tutorials and guides

## Tutorials

[My requests in Jira Service Desk](/cloud/jira/service-desk/my-requests-in-jira-service-desk/)

## Guides

[Atlassian Connect app migration guide to improve user privacy](/cloud/jira/service-desk/connect-app-migration-guide/)

[Build a Jira app using a framework](/cloud/jira/service-desk/build-a-jira-app-using-a-framework/)

[Creating Jira Service Desk requests from Twitter](/cloud/jira/service-desk/creating-jira-service-desk-requests-from-twitter/)

[Creating your own branded customer portal](/cloud/jira/service-desk/creating-your-own-branded-customer-portal/)

[Exploring the Jira Service Desk domain model via the REST APIs](/cloud/jira/service-desk/exploring-the-jira-service-desk-domain-model-via-the-rest-apis/)

[Extending the agent view](/cloud/jira/service-desk/extending-the-agent-view/)

[Implementing automation actions](/cloud/jira/service-desk/implementing-automation-actions/)
