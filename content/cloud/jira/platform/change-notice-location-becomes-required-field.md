---
title: "Change notice - Location will become a required field in board creation"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2018-03-22"
---

# Change notice - Location will become a required field in board creation

When you create a board in Jira, you are required to provide a [location](https://confluence.atlassian.com/jirasoftwarecloud/boards-in-the-new-jira-experience-937886048.html) (either a project or user). This behavior will soon extend to the Jira Software REST API, and [requests to create a board](https://developer.atlassian.com/cloud/jira/software/rest/#api-board-post) will require a location in order to succeed.

If you don't provide a `location` body parameter, POST requests to `/board` will fail with HTTP response 400.

Effective date: 03 Sep 2018