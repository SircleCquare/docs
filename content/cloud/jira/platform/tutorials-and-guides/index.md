---
title: Tutorials and guides
product: jiracloud
category: devguide
layout: tutorials-and-guides
date: "2019-04-24"
---

# Tutorials and guides

## Tutorials

[Jira activity](/cloud/jira/platform/project-activity/)

## Guides

[Atlassian Connect app migration guide to improve user privacy](/cloud/jira/platform/connect-app-migration-guide/)

[Build a Jira app using a framework](/cloud/jira/platform/build-a-jira-app-using-a-framework/)

[Integrate Jira issues with your application](/cloud/jira/platform/integrate-jira-issues-with-your-application/)

[Using the issue field module](/cloud/jira/platform/using-the-issue-field-module/)

[Using the project page module](/cloud/jira/platform/using-the-project-page-module/)
