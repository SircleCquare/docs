---
title: "OAuth 2.0 authorization code grants (3LO) for apps"
platform: cloud
product: jswcloud
category: devguide
subcategory: learning
date: "2018-09-01"
---

# OAuth 2.0 authorization code grants (3LO) for apps

{{% warning %}}
OAuth 2.0 authorization code grants (3LO) for apps is currently not available for Jira Software Cloud.
{{% /warning %}}
