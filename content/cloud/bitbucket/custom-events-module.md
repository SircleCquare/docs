---
title: "Custom events module"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: learning 
date: "2018-07-23"
---

# Custom events module

In addition to subscribing to Bitbucket webhooks,
an app can also broadcast its own events (AKA webhooks) for other apps to consume.
This is useful as a way for one app to notify another app of a particular event.

{{% note %}}
This guide explains how an app can broadcast a custom event for other apps to consume.
If you're looking for information about subscribing to event notifications
read about using the [webhooks module](/cloud/bitbucket/modules/webhook) instead.
{{% /note %}}

For this example we're creating an app to tell time called "Big Ben."
The app will notify subscribed apps every time an hour passes
by sending an event that looks like this:

``` json
{
  "utcTime": "2018-07-19T20:08:31Z"
}
```

## Adding a customEvent module

The first step to sending a custom event is to define the app's events
in the [customEvents module](/cloud/bitbucket/modules/custom-events) in the app's descriptor file.
The `customeEvents` module should be a dictionary where there is one unique key for each event.
Each event requires a `schema` property that defines the [JSON schema](http://json-schema.org/)
of the event payload.

To define the event above, we put the following in our descriptor:

``` json
{
  "key": "big-ben",
  "modules": {
    "customEvents": {
      "big-ben:bong": {
        "schema": {
          "type": "object",
          "properties": {
            "utcTime": {
              "type": "string"
            }
          }
        }
      }
    }
  }
}
```

The name of the event is `big-ben:bong`.
The event name must start with the app key,
followed by a colon and then the name of your event.

The schema for the "bong" event, reproduced below,
specifies that the `big-ben:bong` event payload
will be an object with one property called `utcTime` which is of type string.

For clarity, here is the schema of our object by itself:

``` json
"schema": {
  "type": "object",
  "properties": {
    "utcTime": {
      "type": "string"
    }
  }
}
```

At this point, this is the minimum amount of information needed in a descriptor to trigger a custom event.
However, we're going to add another event to this example and add some more properties to our schema.

## Adding more than one custom event

Let's say we want to publish another event for every minute that passes.
In the app descriptor, the content of this additional event will be the same,
but we'll enable subscribers to choose the interval at which to receive events:
consumers could choose to receive events every minute, every hour, or both.

To add this new event add another entry to the `customEvents` module:

``` json
{
  "key": "big-ben",
  "modules": {
    "customEvents": {
      "big-ben:bong": {
        "schema": {
          "type": "object",
          "properties": {
            "utcTime": {
              "type": "string"
            }
          }
        }
      },
      "big-ben:tick": {
        "schema": {
          "type": "object",
          "properties": {
            "utcTime": {
              "type": "string"
            }
          }
        }
      }
    }
  }
}
```

## Defining app schemas in "definitions"

You'll notice here that we are using the same schema twice.
We can eliminate this duplication by defining our object types in
the app level [definitions](/cloud/bitbucket/app-descriptor/#definitions) property.
This enables the object schemas to be shared between different events,
and for use by other modules like the [proxy module](/cloud/bitbucket/modules/api-proxy/).

``` json
{
  "key": "big-ben",
  "definitions": {
    "big-ben:timestamp": {
      "type": "object",
      "properties": {
        "utcTime": {
          "type": "string",
          "format": "date"
        }
      }
    }
  },
  "modules": {
    "customEvents": {
      "big-ben:bong": {
        "schema": {
          "$ref": "#/definitions/big-ben:timestamp"
        }
      },
      "big-ben:tick": {
        "schema": {
          "$ref": "#/definitions/big-ben:timestamp"
        }
      }
    }
  }
}
```

In the above example we specify a new JSON schema definition for our app called `big-ben:timestamp`.
Notice that the object definition is also prepended with the app key, just like the event names.
JSON schema also supports some string formats, so we can add `"format": "date"` to be even more specific about our schema.

## Referencing Bitbucket types in app schemas

An app's data is likely to be related to an object in Bitbucket,
in which case you can reference that object in your schema:

``` json
{
  "key": "big-ben",
  "definitions": {
    "big-ben:timestamp": {
      "type": "object",
      "properties": {
        "utcTime": {
          "type": "string",
          "format": "date"
        },
        "repository": {
          "$ref": "#/definitions/repository"
        }
      }
    }
  },
  "modules": {
    "customEvents": {
      "big-ben:bong": {
        "schema": {
          "$ref": "#/definitions/big-ben:timestamp"
        }
      },
      "big-ben:tick": {
        "schema": {
          "$ref": "#/definitions/big-ben:timestamp"
        }
      }
    }
  }
}
```

In the above example, our `big-ben:timestamp` definition now has a property for `utcTime` and `repository`.
The type of Bitbucket object that you reference depends on what you want your app to do.
For a full list of available objects see
the page about [object hydration](/cloud/bitbucket/proxy-object-hydration/).

{{% tip %}}
Schema definitions are not currently used to validate the payload of triggered custom events
but app developers should strive to make sure that they do.
{{% /tip %}}

## Triggering a custom event

Once we've defined the `customEvents` module, including what the shape of its content will be,
now we need our app to trigger the event as necessary.
We'll need to trigger `big-ben:bong` on every hour and `big-ben:tick` on every minute.

Since webhooks are subscribed to at the account-level, they must be triggered on an account as well.
To trigger your custom event for a particular account you need to make an HTTP POST request to the
`/2.0/addon/users/{target_user}/events/{event_key}` API
where `{target_user}` specifies an account (either by account ID or UUID)
and `{event_key}` is what's defined in our app descriptor.

[See an example of how to use this resource here.](/bitbucket/api/2/reference/resource/addon/users/%7Btarget_user%7D/events/%7Bevent_key%7D)

The body of the POST would look something like this:

``` json
{
  "utcTime": "2018-07-19T20:08:31Z",
  "repository": {
    "type": "repository",
    "uuid": "{02bcc896-a8f0-4e5f-8387-27daf045ef65}",
  }
}
```

## Hydrating Bitbucket types in the event

Bitbucket will hydrate objects it knows about before sending a response to recipients.
In the last example, only a type and UUID were provided for the repository object,
but more of the corresponding data for that object will be added dynamically
so that recipients receive all of the default repository data:

``` json
{
  "utcTime": "2018-07-19T20:08:31Z",
  "repository": {
    "scm": "git",
    "website": "",
    "has_wiki": false,
    "uuid": "{02bcc896-a8f0-4e5f-8387-27daf045ef65}",
    "links": {
      "watchers": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs/watchers"
      },
      "branches": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs/refs/branches"
      },
      "tags": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs/refs/tags"
      },
      "commits": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs/commits"
      },
      "clone": [
        {
          "href": "https://seanaty@bitbucket.org/atlassian-developers/docs.git",
          "name": "https"
        },
        {
          "href": "git@bitbucket.org:atlassian-developers/docs.git",
          "name": "ssh"
        }
      ],
      "self": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs"
      },
      "source": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs/src"
      },
      "html": {
        "href": "https://bitbucket.org/atlassian-developers/docs"
      },
      "avatar": {
        "href": "https://bytebucket.org/ravatar/%7B02bcc896-a8f0-4e5f-8387-27daf045ef65%7D?ts=markdown"
      },
      "hooks": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs/hooks"
      },
      "forks": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs/forks"
      },
      "downloads": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs/downloads"
      },
      "pullrequests": {
        "href": "https://api.bitbucket.org/2.0/repositories/atlassian-developers/docs/pullrequests"
      }
    },
    "fork_policy": "allow_forks",
    "name": "docs",
    "project": {
      "key": "DOC",
      "type": "project",
      "uuid": "{8addfcd5-2b65-495b-966f-576c915ba1cf}",
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/teams/atlassian-developers/projects/DOC"
        },
        "html": {
          "href": "https://bitbucket.org/account/user/atlassian-developers/projects/DOC"
        },
        "avatar": {
          "href": "https://bitbucket.org/account/user/atlassian-developers/projects/DOC/avatar/32"
        }
      },
      "name": "Documentation"
    },
    "language": "markdown",
    "created_on": "2017-01-03T13:13:49.409012+00:00",
    "mainbranch": {
      "type": "branch",
      "name": "master"
    },
    "full_name": "atlassian-developers/docs",
    "has_issues": false,
    "owner": {
      "username": "atlassian-developers",
      "display_name": "Atlassian Developers",
      "type": "team",
      "uuid": "{fe448b51-a0cd-4684-b627-48b074d3a262}",
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/teams/atlassian-developers"
        },
        "html": {
          "href": "https://bitbucket.org/atlassian-developers/"
        },
        "avatar": {
          "href": "https://bitbucket.org/account/atlassian-developers/avatar/"
        }
      }
    },
    "updated_on": "2018-07-24T01:13:30.054461+00:00",
    "size": 121438314,
    "type": "repository",
    "slug": "docs",
    "is_private": false,
    "description": "Atlassian developer documentation (developer.atlassian.com)"
  }
}
```

## Subscribing to a custom webhook

To test that a custom event works properly you need to first subscribe to it.

You subscribe to a custom event by creating another Connect app with a webhook module.
Read more about how to do this in the [Webhooks module documentation](/cloud/bitbucket/modules/webhook).

Here is an example of what to put in the app descriptor to subscribe to a custom event:

``` json
"modules": {
  "webhooks": [
    {
      "event": "big-ben:bong",
      "url": "/webhook"
    },
    {
      "event": "big-ben:tick",
      "url": "/webhook"
    }
  ]
}
```


## Summary

Just to re-cap, here are the steps to gettting started sending a custom event.

1. Define your event in the [`customEvents` module](/cloud/bitbucket/modules/custom-events).
1. Define your definitions in the [`definitions` section of the descriptor](/cloud/bitbucket/app-descriptor/#definitions) (optional).
1. Trigger the custom event by [making an HTTP post](/bitbucket/api/2/reference/resource/addon/users/%7Btarget_user%7D/events/%7Bevent_key%7D).
1. Subscribe to the custom event by defining a [`webhooks` module](/cloud/bitbucket/modules/webhook/).
1. Profit.
