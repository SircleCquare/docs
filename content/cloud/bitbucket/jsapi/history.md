---
title: "History"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: javascript 
date: "2017-05-18"
---

# History

The `history` API allows your app to manipulate the current page URL for use in navigation

{{% note %}}This is only enabled for page modules (Admin page, General page, Configure page, User profile page).{{% /note %}}

## Example

``` javascript
AP.require([&quot;history&quot;], function(history){

   // Register a function to run when state is changed.
   // You should use this to update your UI to show the state.
   history.popState(function(e){
       alert(&quot;The URL has changed from: &quot; + e.oldURL + &quot;to: &quot; + e.newURL);
   });

   // Adds a new entry to the history and changes the url in the browser.
   history.pushState(&quot;page2&quot;);

   // Changes the URL back and invokes any registered popState callbacks.
   history.back();

});
```

## Methods

### back ( )

Goes back one step in the joint session history. Will invoke the popstate callback.

#### Example
        
``` javascript
AP.require(["history"], function(history){
   history.back(); // go back by 1 entry in the browser history.
});
```

### forward ( )

Goes back one step in the joint session history. Will invoke the popstate callback.
    
#### Example
        
``` javascript
AP.require(["history"], function(history){
   history.forward(); // go forward by 1 entry in the browser history.
});
```

### getState ( )

The current URL anchor.
    
**Returns**: String

#### Example
        
``` javascript
AP.require(["history"], function(history){
   history.pushState("page5");
   history.getState(); // returns "page5";
});
```

### go (int)

Goes back or forward the specified number of steps. A zero delta will reload the current page. If the delta is out of range, it does nothing. Will invoke the popstate callback.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>

	<tbody>
        <tr>
                <td ><code>int</code></td>

            <td>
            </td>
            <td><p>delta</p></td>
        </tr>
	</tbody>
</table>

#### Example
        
``` javascript
AP.require(["history"], function(history){
   history.go(-2); // go back by 2 entries in the browser history.
});
```
### popState (Function)

Register a function to be executed on state change.
   
#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>

	<tbody>
        <tr>
                <td><code>Function</code></td>
            <td>
            </td>
            <td>callback to be executed on state change</td>
        </tr>
	</tbody>
</table>

### pushState (String)

Pushes the given data onto the session history. Does NOT invoke `popState` callback.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>

		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>String</code></td>
            <td>

            </td>
            <td><p>URL to add to history</p></td>
        </tr>
	</tbody>
</table>

### replaceState (String)

Updates the current entry in the session history. Does NOT invoke `popState` callback.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>

	<tbody>
        <tr>
                <td><code>String</code></td>
            <td>
            </td>
            <td>URL to add to history</td>
        </tr>
	
	</tbody>
</table>
