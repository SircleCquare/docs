---
title: "App properties API"
platform: cloud
product: confcloud
category: reference
subcategory: appapi
date: "2018-06-13"
---
# App properties API

Atlassian Connect provides a set of REST APIs specifically designed for use by apps.

<a name="get-addons-addonkey-properties" />

## Get app property keys

GET /wiki/rest/atlassian-connect/1/addons/{addonKey}/properties

Returns a list of property keys for the given app key.

#### Parameters

<table class="params table table-striped">
  <thead>
    <tr>
      <th>Location</th>
      <th>Name</th>
      <th>Type</th>
      <th class="last">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="location">
        Path
      </td>
      <td class="name">
        <code>addonKey</code>
      </td>
      <td class="type">
        <code>string</code>
      </td>
      <td class="description last">
        The key of the app, as defined in its descriptor
      </td>
    </tr>
  </tbody>
</table>

#### Response representations

##### `200` - application/json

```json
{
  "keys" : [
        {
          "key" : "first_key",
          "self" : "https://your-site.atlassian.net/wiki/rest/atlassian-connect/1/addons/my-add-on/properties/first_key"
        },
        {
          "key" : "another_key",
          "self" : "https://your-site.atlassian.net/wiki/rest/atlassian-connect/1/addons/my-add-on/properties/another_key"
        }
  ]
}
```

##### `401` - application/json

Request without credentials or with invalid credentials, e.g., by an uninstalled app.

##### `404` - application/json

Request issued by a user with insufficient credentials, e.g., for an app's data by anyone but the app itself,
or for a app that does not exist.

<a name="get-addons-addonkey-properties-propertykey" />

## Get app property 

GET /wiki/rest/atlassian-connect/1/addons/{addonKey}/properties/{propertyKey}

Returns an app property for the given property key.

#### Parameters

<table class="params table table-striped">
  <thead>
    <tr>
      <th>Location</th>
      <th>Name</th>
      <th>Type</th>
      <th class="last">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="location">
        Path
      </td>
      <td class="name">
        <code>addonKey</code>
      </td>
      <td class="type">
        <code>string</code>
      </td>
      <td class="description last">
        The key of the app, as defined in its descriptor
      </td>
    </tr>
    <tr>
      <td class="location">
        Path
      </td>
      <td class="name">
        <code>propertyKey</code>
      </td>
      <td class="type">
        <code>string</code>
      </td>
      <td class="description last">
        The key of the property
      </td>
    </tr>
    <tr>
      <td class="location">
        Query
      </td>
      <td class="name">
        <code>jsonValue</code>
      </td>
      <td class="type">
        <code>boolean</code>
      </td>
      <td class="description last">
        <p>Set to <code>true</code> to have the <code>value</code> field return as a JSON object. If false it will return the <code>value</code> field
          as a string containing escaped JSON. This will be deprecated during June 2016 and the behavior will default
          to true.
        </p>
      </td>
    </tr>
  </tbody>
</table>

#### Response representations

##### `200` - application/json

```json
{
  "key" : "abcd",
  "value" : true,
  "self" : "https://your-site.atlassian.net/wiki/rest/atlassian-connect/1/addons/my-add-on/properties/abcd"
}
```

##### `400` - application/json

Property key longer than 127 characters.

##### `401` - application/json

Request without credentials or with invalid credentials, e.g., by an uninstalled app.

##### `404` - application/json

Request to get a property that does not exist.

##### `404` - application/json

Request issued by a user with insufficient credentials, e.g., for an app's data by anyone but the app itself,
or for a app that does not exist.

<a name="put-addons-addonkey-properties-propertykey" />

## Create or update app property

PUT /wiki/rest/atlassian-connect/1/addons/{addonKey}/properties/{propertyKey}

Creates or updates a property.

#### Parameters

<table class="params table table-striped">
  <thead>
    <tr>
      <th>Location</th>
      <th>Name</th>
      <th>Type</th>
      <th class="last">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="location">
        Path
      </td>
      <td class="name">
        <code>addonKey</code>
      </td>
      <td class="type">
        <code>string</code>
      </td>
      <td class="description last">
        The key of the app, as defined in its descriptor
      </td>
    </tr>
    <tr>
      <td class="location">
        Path
      </td>
      <td class="name">
        <code>propertyKey</code>
      </td>
      <td class="type">
        <code>string</code>
      </td>
      <td class="description last">
        The key of the property
      </td>
    </tr>
  </tbody>
</table>

#### Response 

##### `200` - application/json

Property updated.

##### `201` - application/json

Property created.

##### `400` - application/json

Property key longer than 127 characters.

##### `400` - application/json

Request made with invalid JSON.

##### `401` - application/json

Request without credentials or with invalid credentials, e.g., by an uninstalled app.

##### `404` - application/json

Request to get a property that does not exist.

##### `404` - application/json

Request issued by a user with insufficient credentials, e.g., for an app's data by anyone but the app itself,
or for a app that does not exist.

<a name="delete-addons-addonkey-properties-propertykey" />

## Delete app property

DELETE /wiki/rest/atlassian-connect/1/addons/{addonKey}/properties/{propertyKey}

Deletes an app property.

#### Parameters

<table class="params table table-striped">
  <thead>
    <tr>
      <th>Location</th>
      <th>Name</th>
      <th>Type</th>
      <th class="last">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="location">
        Path
      </td>
      <td class="name">
        <code>addonKey</code>
      </td>
      <td class="type">
        <code>string</code>
      </td>
      <td class="description last">
        The key of the app, as defined in its descriptor
      </td>
    </tr>
    <tr>
      <td class="location">
        Path
      </td>
      <td class="name">
        <code>propertyKey</code>
      </td>
      <td class="type">
        <code>string</code>
      </td>
      <td class="description last">
        The key of the property
      </td>
    </tr>
  </tbody>
</table>

#### Response representations

##### `204` - application/json

Property deleted.

##### `400` - application/json

Property key longer than 127 characters.

##### `401` - application/json

Request without credentials or with invalid credentials, e.g., by an uninstalled app.

##### `404` - application/json

Request to get a property that does not exist.

##### `404` - application/json

Request issued by a user with insufficient credentials, e.g., for an app's data by anyone but the app itself,
or for a app that does not exist.
